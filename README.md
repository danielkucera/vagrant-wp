This project can be used to automatically provision and start wordpress site.
It uses Vagrant for VM creation and ansible for provisioning. 

## Installation
```
git clone https://bitbucket.org/danielkucera/vagrant-wp
vagrant up
```
You will be asked for vault password so you should better have it. You can get it from project maintainer.
After completion your WP is available on http://192.168.33.10/ .

## Configuration
You can edit configuration variables in wordpress.yml.
After editing, you can call `vagrant provision` to apply changes.

## Networking
The VM is configured to be accessible on IP 192.168.33.10 from machine hosting it. For different networking options see Vagrant networking: https://www.vagrantup.com/docs/networking/

## TODO
- store wordpress salts for reuse - now they are regenerated on each run
- more config variables (wordpress location, db user, db name)
- possibility to provision DB into different VM

